var sys = require('sys')
var exec = require('child_process').exec;

var readline = require('readline');
var async = require('async');
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var blessed = require('blessed')
  , contrib = require('blessed-contrib')

var questionPool = [
    "Can you explain accelerationism?",
    "Can you explain the technological singularity?",
    "Is the singularity necessarily technological?",
    "What about issues of race and gender?",
    "Will the benefits be only for the rich?",
    "What of sex and intimacy?",
    "What about the dangers of creating intelligence smarter than us?",
    "Are you a Marxist?",
    "Can humans change the course of acceleration?"
];

var availableQuestions = [0,1];


var st0 = "\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557\u2588\u2588\u2588\u2588\u2588\u2588\u2557  \u2588\u2588\u2588\u2588\u2588\u2588\u2557  \u2588\u2588\u2588\u2588\u2588\u2588\u2557\u2588\u2588\u2557  \u2588\u2588\u2557    \u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557\r\n\u2588\u2588\u2554\u2550\u2550\u2550\u2550\u255D\u2588\u2588\u2554\u2550\u2550\u2588\u2588\u2557\u2588\u2588\u2554\u2550\u2550\u2550\u2588\u2588\u2557\u2588\u2588\u2554\u2550\u2550\u2550\u2550\u255D\u2588\u2588\u2551  \u2588\u2588\u2551    \u2588\u2588\u2554\u2550\u2550\u2550\u2550\u255D\r\n\u2588\u2588\u2588\u2588\u2588\u2557  \u2588\u2588\u2588\u2588\u2588\u2588\u2554\u255D\u2588\u2588\u2551   \u2588\u2588\u2551\u2588\u2588\u2551     \u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2551    \u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557\r\n\u2588\u2588\u2554\u2550\u2550\u255D  \u2588\u2588\u2554\u2550\u2550\u2550\u255D \u2588\u2588\u2551   \u2588\u2588\u2551\u2588\u2588\u2551     \u2588\u2588\u2554\u2550\u2550\u2588\u2588\u2551    \u255A\u2550\u2550\u2550\u2550\u2588\u2588\u2551\r\n\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557\u2588\u2588\u2551     \u255A\u2588\u2588\u2588\u2588\u2588\u2588\u2554\u255D\u255A\u2588\u2588\u2588\u2588\u2588\u2588\u2557\u2588\u2588\u2551  \u2588\u2588\u2551    \u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2551\r\n\u255A\u2550\u2550\u2550\u2550\u2550\u2550\u255D\u255A\u2550\u255D      \u255A\u2550\u2550\u2550\u2550\u2550\u255D  \u255A\u2550\u2550\u2550\u2550\u2550\u255D\u255A\u2550\u255D  \u255A\u2550\u255D    \u255A\u2550\u2550\u2550\u2550\u2550\u2550\u255D\r\n                                                     ";
var st1 = "Welcome back, returning you to #SINGULARITY\r\n...\r\n>Please enter your username:";
var st2 = ">\r\n>\u2500\u253C\u2500\u253C\u2500\u2554\u2550\u2557\u2566\u2554\u2557\u2554\u2554\u2550\u2557\u2566 \u2566\u2566  \u2554\u2550\u2557\u2566\u2550\u2557\u2566\u2554\u2566\u2557\u2566 \u2566\r\n>\u2500\u253C\u2500\u253C\u2500\u255A\u2550\u2557\u2551\u2551\u2551\u2551\u2551 \u2566\u2551 \u2551\u2551  \u2560\u2550\u2563\u2560\u2566\u255D\u2551 \u2551 \u255A\u2566\u255D\r\n>     \u255A\u2550\u255D\u2569\u255D\u255A\u255D\u255A\u2550\u255D\u255A\u2550\u255D\u2569\u2550\u255D\u2569 \u2569\u2569\u255A\u2550\u2569 \u2569  \u2569 \r\n>"
var st3 = ">#singularity is a channel dedicated to discussions about the technological singularity.\r\n>We encourage critical conversations but please be polite and follow our rules or you will be BANNED."
var st4 = ">Recent news, Jan 15, 2015; Elon Musk Tweets, \"Funding research on artificial intelligence safety. It's all fun & games until someone loses an I http://futureoflife.org/misc/AI\""
var st5 = "YakRulerWiz> You must be new..."
var st6 = "YakRulerWiz> Technology is progressing at an exponential rate. We perceieve progress as linear but this just an illusion of our perception."
var st7 = "YakRulerWiz> It took 2 billion years for us to grow out of the primordial soup. A thousand years from agriculture to industry. A century to switch from production to information technology."
var st8 = "YakRulerWiz> Look where technology has taken us now. We are at the cusp of EPOCH 5, the merger of human intelligence and technology. This is the Technology singularity."
var st9 = "YakRulerWiz> It's implications are profound."
var st10 = "YakRulerWiz> What do you believe?"

/*
rl.question(st1, function(answer) {
    // TODO: Log the answer in a database
    console.log(">Welcome " + answer + " to #singularity.");

    rl.close();
    console.log(st2);
});*/


//TODO this is wrong
var promptQuestion = function(question,chcs,callback) {
    var printChoices = function()
    {
        for(var i = 0; i < chcs.length; i++)
        {
            console.log((i+1) + "): " + chcs[i]);
        }
    }
    var getAnswer = function()
    {
        rl.question("?:",function(answer){
            var asint = parseInt(answer)
            if(isNaN(asint) || asint <= 0 || asint > chcs.length) {
                getAnswer();
            } else {
                callback(asint);
            }
        });
    }
    console.log(chcs);
    console.log(question);
    printChoices();
    getAnswer();
}

console.log('\033[2J');
console.log(st0);
async.series([
        function(callback){
            rl.question(st1, function(answer){
                console.log(">Welcome " + answer + " to: ");
                rl.close();
                console.log(st2);
                callback(null, 1);
            });
        },
        function(callback){
            setTimeout(function(){
                console.log(st3);
                console.log(st4);
                callback(null, 2);
            }, 1000);
        },
        function(callback){
            setTimeout(function(){
                console.log(st5);
                callback(null, 3);
            }, 5000);
        },
        function(callback){
            setTimeout(function(){
                console.log(st6);
                callback(null, 4);
            }, 5000);
        },
        function(callback){
            setTimeout(function(){
                console.log(st7);
                callback(null, 5);
            }, 8000);
        },
        function(callback){
            setTimeout(function(){
                console.log(st8);
                callback(null, 6);
            }, 8000);
        },
        function(callback){
            setTimeout(function(){
                console.log(st9);
                callback(null, 7);
            }, 8000);
        },
        function(callback){
            setTimeout(function(){
                console.log(st10);
                callback(null, 8);
            }, 8000);
        },
        function(callback){
            setTimeout(function(){
                console.log("YRZ has sent you EPOCH5.mp4");
                callback(null, 9);
            }, 8000);
        },
        function(callback){
            setTimeout(function(){
                exec("open CONTENT/videos/");
                callback(null, 10);
            }, 2000);
        }],
    function(err, results) {
        // results is now equal to: {one: 1, two: 2}
    }
);

/*
async.series([
        function(callback){
            promptQuestion("aouou???", ["Saab", "Volvo", "BMW"], function(answer){
                console.log("you have selected " + answer);
                callback(null,1)
            });
        },
        function(callback){
            rl.question(st1, function(answer){
                console.log(">Welcome " + answer + " to #singularity.");
                rl.close();
                console.log(st2);
                callback(null, 2);
            });
        },
        function(callback){
            setTimeout(function(){
                console.log("aoeuoau");
                callback(null, 3);
            }, 1000);
        }],
    function(err, results) {
        // results is now equal to: {one: 1, two: 2}
    }
);

*/
/*



var playerName;


rl.question(st1, function(answer) {
    playerName = answer;
    console.log("Thank you for your valuable feedback:", answer);

    console.log(st2 + answer);
    console.log(st1);
    rl.close();
});
*/