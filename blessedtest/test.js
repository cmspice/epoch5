var blessed = require('blessed');

// Create a screen object.
var screen = blessed.screen({
  autoPadding: true,
  smartCSR: true
});

screen.title = 'my window title';

// Create a box perfectly centered horizontally and vertically.
var box = blessed.scrollabletext({  
  parent: screen,
  mouse: true,
  keys: true,
  vi: true,
  border: {
    type: 'line',
    fg: '#00ff00'
  },
  scrollbar: {
    fg: 'blue',
    ch: '|'
  },
  width: '50%',
  height: '50%',
  top: 'center',
  left: 'center',
  align: 'center',
  content: 'Loading...',
  tags: true
});

// Append our box to the screen.
screen.append(box);

// Add a PNG icon to the box (X11 only)
var icon = blessed.image({
  parent: box,
  top: 0,
  left: 0,
  width: 'shrink',
  height: 'shrink',
  file: __dirname + '/my-program-icon.png',
  search: false
});

// If our box is clicked, change the content.
box.on('click', function(data) {
  box.setContent('{center}Some different {red-fg}content{/red-fg}.{/center}');
  screen.render();
});

// If box is focused, handle `enter`/`return` and give us some more content.
box.key('enter', function(ch, key) {
  //box.setContent('{right}Even different {black-fg}content{/black-fg}.{/right}\n');
  //box.setLine(1, 'bar');
  box.insertLine(1, 'foo');
  screen.render();
});

// Quit on Escape, q, or Control-C.
screen.key(['escape', 'q', 'C-c'], function(ch, key) {
  return process.exit(0);
});

// Focus our element.
box.focus();

// Render the screen.
screen.render();


// http://nodejs.org/api.html#_child_processes
var sys = require('sys')
var exec = require('child_process').exec;
//exec("open ./")

// executes `pwd`
/*child = exec("pwd", function (error, stdout, stderr) {
  sys.print('stdout: ' + stdout);
  sys.print('stderr: ' + stderr);
  if (error !== null) {
    console.log('exec error: ' + error);
  }
});*/